module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: '\n'
            },
            dist: {
                files: [{
                    src: ['assets/js/custom/*.js'],
                    dest: 'assets/js/app.js'
                }, {
                    src: ['assets/js/lib/*.js'],
                    dest: 'assets/js/lib.js'
                }],
            },
        },
        uglify: {
            min: {
                files: {
                    'assets/js/app.js': ['assets/js/app.js']
                }
            }
        },
        sass: {
            options: {},
            dist: {
                options: {
                    outputStyle: 'compressed',
                    precision: 5
                },
                files: {
                    'assets/css/app.css': 'assets/scss/app.scss'
                }
            }
        },
        // Uncomment if the css file is too big for IE9
        // csssplit: {
        //     dist: {
        //         src: ['assets/css/app.css'],
        //         dest: 'assets/css/app_ie.css',
        //         options: {
        //             suffix: '_'
        //         }
        //     },
        // },
        watch: {
            options: {
                livereload: true
            },
            scripts: {
                files: ['assets/js/custom/*.js'],
                tasks: ['concat', 'uglify']
            },
            sass: {
                files: ['assets/scss/*.scss', 'assets/scss/*/*.scss'],
                tasks: ['sass'],
                options: {
                    livereload: true,
                }
            },
        },
    });

    // Load tasks
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    // grunt.loadNpmTasks('grunt-csssplit'); // FOR IE 9
    // Default task(s).
    // grunt.registerTask('default', ['concat', 'uglify', 'sass', 'csssplit', 'watch']); // FOR IE 9
    grunt.registerTask('default', ['concat', 'uglify', 'sass', 'watch']);
};